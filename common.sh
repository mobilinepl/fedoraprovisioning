#!/bin/sh

sudo dnf install -y git autoconf automake pkgconfig gtk3-devel wget freeze remmina 
sudo dnf install -y pidgin pidgin-sipe purple-hangouts pidgin-hangouts
sudo dnf install -y nautilus-open-terminal nautilus-sendto nautilus-font-manager nautilus-extensions
