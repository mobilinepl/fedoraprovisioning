#!/bin/bash

sudo dnf install -y gstreamer{1,}-{ffmpeg,libav,plugins-{good,ugly,bad{,-free,-nonfree}}} --setopt=strict=0
sudo dnf install -y gstreamer{1,}-{plugin-crystalhd,ffmpeg,plugins-{good,ugly,bad{,-free,-nonfree,-freeworld,-extras}{,-extras}}} libmpg123 lame-libs --setopt=strict=0
sudo dnf remove -y gstreamer1-plugins-ugly
sudo dnf install -y gstreamer1-plugin-mpg123 mpg123-libs
sudo dnf install -y vlc
