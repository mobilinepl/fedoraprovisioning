#!/bin/sh

echo "***************************************"
echo "   UPDATE OS BEFORE INSTALATION VBOX   "
echo "***************************************"

cd /etc/yum.repos.d/
sudo wget http://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo
sudo dnf install -y binutils gcc make patch libgomp glibc-headers glibc-devel kernel-headers kernel-devel dkms
sudo dnf install -y VirtualBox-5.2
sudo /usr/lib/virtualbox/vboxdrv.sh setup
sudo usermod -a -G vboxusers $USER  && newgrp vboxusers
VBoxManage --version
